CREATE TABLE fintech.tb_endereco (
	id bigserial NOT NULL,
	logradouro varchar(100) NOT NULL,
	complemento varchar(100) NULL,
	bairro varchar(30) NOT NULL,
	numero int4 NOT NULL,
	CONSTRAINT tb_emdereco_pkey PRIMARY KEY (id)
);

CREATE TABLE fintech.tb_cartao (
	id bigserial NOT NULL,
	bandeira varchar(10) NULL,
	digito_identificador varchar(9) NULL,
	digito_verificador varchar(1) NULL,
	limite numeric(19,2) NULL,
	prefixo varchar(2) NULL,
	CONSTRAINT tb_cartao_pkey PRIMARY KEY (id)
);

CREATE TABLE fintech.tb_conta (
	id bigserial NOT NULL,
	saldo numeric(19,2) NULL,
	valor numeric(19,2) NULL,
	cartao_id int8 NULL,
	CONSTRAINT tb_conta_pkey PRIMARY KEY (id),
	CONSTRAINT fk_conta_cartao FOREIGN KEY (cartao_id) REFERENCES fintech.tb_cartao(id)
);

CREATE TABLE fintech.tb_cliente (
	id bigserial NOT NULL,
	nome_completo varchar(100) NULL,
	data_nascimento date NULL,
	cpf varchar(11) NULL,
	endereco_id int8,
	conta_id int8,
	CONSTRAINT tb_cliente_pkey PRIMARY KEY (id),
	CONSTRAINT cliente_endereco_fk FOREIGN KEY (endereco_id) REFERENCES fintech.tb_endereco(id),
	CONSTRAINT cliente_conta_fk FOREIGN KEY (conta_id) REFERENCES fintech.tb_conta(id)
);

CREATE TABLE fintech.tb_venda (
	id bigserial NOT NULL,
	servico varchar(50) NOT NULL,
	valor numeric(19,2) NULL,
	cliente_id int8 NULL,
	CONSTRAINT tb_venda_pkey PRIMARY KEY (id),
	CONSTRAINT venda_cliente_fk FOREIGN KEY (cliente_id) REFERENCES fintech.tb_cliente(id)
);

