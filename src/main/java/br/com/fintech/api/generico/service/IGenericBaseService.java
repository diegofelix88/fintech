package br.com.fintech.api.generico.service;

import br.com.fintech.api.generico.model.IGenericBaseModel;

public interface IGenericBaseService<E extends IGenericBaseModel, T> {

	E incluir(E entidade);
	E editar(E entidade) ;
	void excluir(T id);
	E buscarPorId(T id);
	Object listar();
		
}
