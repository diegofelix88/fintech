package br.com.fintech.api.generico.model;

import java.io.Serializable;

public interface IGenericBaseModel {
	
	Serializable getId();
	
}
