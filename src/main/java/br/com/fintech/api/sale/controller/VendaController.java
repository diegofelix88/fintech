package br.com.fintech.api.sale.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fintech.api.generico.controller.GenericBaseController;
import br.com.fintech.api.sale.model.Venda;
import br.com.fintech.api.sale.model.dto.VendaDTO;
import br.com.fintech.api.sale.service.VendaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/venda")
@Api(value = "Venda")
public class VendaController extends GenericBaseController<VendaService, Venda, Long> {
	
	@PostMapping("/novo")
	@ApiOperation(value = "Incluir uma nova venda cartão")
	public ResponseEntity<?> vendaCliente(@RequestBody VendaDTO entity)  {
		return ResponseEntity.ok(servico.vendaCliente(entity));
	}
}
