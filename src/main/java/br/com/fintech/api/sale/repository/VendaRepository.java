package br.com.fintech.api.sale.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.fintech.api.sale.model.Venda;
import br.com.fintech.api.sale.repository.query.VendaRepositoryQuery;

@Repository
public interface VendaRepository extends JpaRepository<Venda, Long>, VendaRepositoryQuery{

}
