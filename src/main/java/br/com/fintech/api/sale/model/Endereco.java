package br.com.fintech.api.sale.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.fintech.api.generico.model.GenericBaseModel;

@Entity
@Table(name = "tb_endereco", schema = "fintech")
public class Endereco extends GenericBaseModel<Long> {
	
	@Size(max = 100)
	@Column(name = "logradouro")
	private String logradouro;
	
	@Size(max = 50)
	@Column(name = "complemento")
	private String complemento;
	
	@Size(max = 50)
	@Column(name = "bairro")
	private String bairro;
	
	@Column(name = "numero")
	private Integer numero;

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
}
