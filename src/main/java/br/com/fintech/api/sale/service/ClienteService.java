package br.com.fintech.api.sale.service;

import java.math.BigDecimal;
import java.util.Random;

import org.springframework.stereotype.Service;

import br.com.fintech.api.generico.service.GenericBaseService;
import br.com.fintech.api.sale.model.Cartao;
import br.com.fintech.api.sale.model.Cliente;
import br.com.fintech.api.sale.model.Conta;
import br.com.fintech.api.sale.model.enums.BandeiraCartaoEnum;
import br.com.fintech.api.sale.repository.ClienteRepository;

@Service
public class ClienteService extends GenericBaseService<ClienteRepository, Cliente, Long> {
	
	public Cliente criarConta(Cliente entity) {
		
		Random digitoVerificador = new Random();
		Random digitoIdentificador = new Random();
		Integer verificador = digitoVerificador.nextInt(999999999);
		Integer identificador = digitoIdentificador.nextInt(9);
		
		Conta conta = new Conta(new Cartao("55", identificador.toString(), verificador.toString(), new BigDecimal(1000.00), BandeiraCartaoEnum.MASTERCARD),
								BigDecimal.ZERO, new BigDecimal(1000.00));
		entity.setConta(conta);
		return repositorio.save(entity);
	}
	
	public Cliente saldoCartao(String cpf, String identificador, String verificador) {
		return repositorio.saldoCartao(cpf, identificador, verificador);
	}
	
	public Cliente buscarPorCpf(String cpf) {
		return repositorio.findByCpf(cpf);
	}

}
