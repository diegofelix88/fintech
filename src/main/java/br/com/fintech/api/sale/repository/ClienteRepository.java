package br.com.fintech.api.sale.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.fintech.api.sale.model.Cliente;
import br.com.fintech.api.sale.repository.query.ClienteRepositoryQuery;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>, ClienteRepositoryQuery{

	public Cliente findByCpf(String cpf);
	
	@Query("SELECT cl FROM Cliente cl INNER JOIN cl.conta ct "
			+ " INNER JOIN ct.cartao ca "
			+ " WHERE cl.cpf = ?1 AND ca.digitoIdentificador = ?2 "
			+ " AND ca.digitoVerificador = ?3 ")
	public Cliente saldoCartao(String cpf, String identificador, String verificador);
}
