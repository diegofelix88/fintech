package br.com.fintech.api.sale.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.fintech.api.generico.model.GenericBaseModel;
import br.com.fintech.api.sale.model.enums.BandeiraCartaoEnum;

@Entity
@Table(name = "tb_cartao", schema = "fintech")
public class Cartao extends GenericBaseModel<Long> {
	
	@Size(max = 2)
	@Column(name = "prefixo")
	private String prefixo;
	
	@Size(max = 1)
	@Column(name = "digito_verificador")
	private String digitoVerificador;
	
	@Size(max = 9)
	@Column(name = "digito_identificador")
	private String digitoIdentificador;
	
	@Column(name = "limite")
	private BigDecimal limite;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "bandeira")
	private BandeiraCartaoEnum bandeira;

	public Cartao() {
		super();
	}

	public Cartao(@Size(max = 2) String prefixo, @Size(max = 1) String digitoVerificador,
			@Size(max = 9) String digitoIdentificador, BigDecimal limite, @Size(max = 10) BandeiraCartaoEnum bandeira) {
		super();
		this.prefixo = prefixo;
		this.digitoVerificador = digitoVerificador;
		this.digitoIdentificador = digitoIdentificador;
		this.limite = limite;
		this.bandeira = bandeira;
	}

	public String getPrefixo() {
		return prefixo;
	}

	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}

	public String getDigitoVerificador() {
		return digitoVerificador;
	}

	public void setDigitoVerificador(String digitoVerificador) {
		this.digitoVerificador = digitoVerificador;
	}

	public String getDigitoIdentificador() {
		return digitoIdentificador;
	}

	public void setDigitoIdentificador(String digitoIdentificador) {
		this.digitoIdentificador = digitoIdentificador;
	}

	public BigDecimal getLimite() {
		return limite;
	}

	public void setLimite(BigDecimal limite) {
		this.limite = limite;
	}

	public BandeiraCartaoEnum getBandeira() {
		return bandeira;
	}

	public void setBandeira(BandeiraCartaoEnum bandeira) {
		this.bandeira = bandeira;
	}

}
