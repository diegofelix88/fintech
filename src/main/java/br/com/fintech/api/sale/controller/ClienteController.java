package br.com.fintech.api.sale.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fintech.api.generico.controller.GenericBaseController;
import br.com.fintech.api.sale.model.Cliente;
import br.com.fintech.api.sale.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/cliente")
@Api(value = "Cliente")
public class ClienteController extends GenericBaseController<ClienteService, Cliente, Long> {
	
	@PostMapping("/novo")
	@ApiOperation(value = "Criar conta cliente")
	public ResponseEntity<?> criarConta(@RequestBody Cliente entity)  {
		return ResponseEntity.ok(servico.criarConta(entity));
	}
}
