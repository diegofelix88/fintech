package br.com.fintech.api.sale.model;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.fintech.api.generico.model.GenericBaseModel;

@Entity
@Table(name = "tb_cliente", schema = "fintech")
public class Cliente extends GenericBaseModel<Long> {
	
	@Size(max = 100)
	@Column(name = "nome_completo")
	private String nomeCompleto;
	
	@Column(name = "data_nascimento")
	private LocalDate dataNascimento;
	
	@Size(max = 11)
	@Column(name = "cpf")
	private String cpf;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "endereco_id", referencedColumnName = "id")
	private Endereco endereco;
	
	@JsonIgnore
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "conta_id", referencedColumnName = "id")
	private Conta conta;
	
	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

}
