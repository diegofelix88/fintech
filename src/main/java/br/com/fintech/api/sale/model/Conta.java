package br.com.fintech.api.sale.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.fintech.api.generico.model.GenericBaseModel;

@Entity
@Table(name = "tb_conta", schema = "fintech")
public class Conta extends GenericBaseModel<Long> {	
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "cartao_id", referencedColumnName = "id")
	private Cartao cartao;
	
	@Column(name = "valor")
	private BigDecimal valor;
	
	@Column(name = "saldo")
	private BigDecimal saldo;

	public Conta() {
		super();
	}

	public Conta(Cartao cartao, BigDecimal valor, BigDecimal saldo) {
		super();
		this.cartao = cartao;
		this.valor = valor;
		this.saldo = saldo;
	}

	public Cartao getCartao() {
		return cartao;
	}

	public void setCartao(Cartao cartao) {
		this.cartao = cartao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	} 
	
}
