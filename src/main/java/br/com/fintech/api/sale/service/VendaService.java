package br.com.fintech.api.sale.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fintech.api.config.errors.AplicationException;
import br.com.fintech.api.generico.service.GenericBaseService;
import br.com.fintech.api.sale.model.Cartao;
import br.com.fintech.api.sale.model.Cliente;
import br.com.fintech.api.sale.model.Venda;
import br.com.fintech.api.sale.model.dto.VendaDTO;
import br.com.fintech.api.sale.repository.VendaRepository;

@Service
public class VendaService extends GenericBaseService<VendaRepository, Venda, Long> {
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private CartaoService cartaoService;
	
	public Venda vendaCliente(VendaDTO entity) {
		Cartao cartao = cartaoService.buscarPorNumeroConta(entity.getNumeroIdentificadorCartao(), entity.getNumeroVerificadorCartao());
		if(cartao == null) {
	          throw new AplicationException("Cartão inválido!");
		}
		
		Cliente cliente = clienteService.buscarPorCpf(entity.getCpfCliente());
		if(cartao.getLimite().doubleValue() < entity.getValor().doubleValue()) {
	           throw new AplicationException("Saldo insuficiente!");
		}
		calcularSaldo(entity, cartao);
		return repositorio.save(new Venda(cliente, entity.getServicoVenda(), entity.getValor()));
	}
	
	private void calcularSaldo(VendaDTO entity, Cartao cartao) {
		cartao.setLimite(cartao.getLimite().subtract(entity.getValor()));
		cartaoService.incluir(cartao);
	}
}
