package br.com.fintech.api.sale.service;

import org.springframework.stereotype.Service;

import br.com.fintech.api.generico.service.GenericBaseService;
import br.com.fintech.api.sale.model.Conta;
import br.com.fintech.api.sale.repository.ContaRepository;

@Service
public class ContaService extends GenericBaseService<ContaRepository, Conta, Long> {
	
	public Conta cargaCartao(Conta entity) {
		return repositorio.save(entity);
	}
}
