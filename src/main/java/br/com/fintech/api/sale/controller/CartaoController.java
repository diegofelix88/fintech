package br.com.fintech.api.sale.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fintech.api.generico.controller.GenericBaseController;
import br.com.fintech.api.sale.model.Cartao;
import br.com.fintech.api.sale.model.dto.CartaoDTO;
import br.com.fintech.api.sale.service.CartaoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/cartao")
@Api(value = "Cartao")
public class CartaoController extends GenericBaseController<CartaoService, Cartao, Long> {
	
	@PutMapping("/saldo")
	@ApiOperation(value = "Incluir saldo cartão")
	public ResponseEntity<?> criarConta(@RequestBody CartaoDTO entity)  {
		return ResponseEntity.ok(servico.saldoCartao(entity));
	}
	
	@GetMapping("/saldo/{identificador}/{verificador}")
	@ApiOperation(value = "Consultar saldo cartão de crédito")
	public ResponseEntity<?> consultarSaldo(@PathVariable("identificador") String identificador, @PathVariable("verificador") String verificador)  {
		return ResponseEntity.ok(servico.buscarPorNumeroConta(identificador, verificador));
	} 
}
