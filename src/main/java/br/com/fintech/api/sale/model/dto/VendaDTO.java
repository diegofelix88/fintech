package br.com.fintech.api.sale.model.dto;

import java.math.BigDecimal;

public class VendaDTO {
	
	private String cpfCliente;
	
	private String servicoVenda;
	
	private String numeroIdentificadorCartao;
	
	private String numeroVerificadorCartao;
	
	private BigDecimal valor;

	public String getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	public String getServicoVenda() {
		return servicoVenda;
	}

	public void setServicoVenda(String servicoVenda) {
		this.servicoVenda = servicoVenda;
	}

	public String getNumeroIdentificadorCartao() {
		return numeroIdentificadorCartao;
	}

	public void setNumeroIdentificadorCartao(String numeroIdentificadorCartao) {
		this.numeroIdentificadorCartao = numeroIdentificadorCartao;
	}

	public String getNumeroVerificadorCartao() {
		return numeroVerificadorCartao;
	}

	public void setNumeroVerificadorCartao(String numeroVerificadorCartao) {
		this.numeroVerificadorCartao = numeroVerificadorCartao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

}
