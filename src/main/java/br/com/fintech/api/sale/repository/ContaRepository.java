package br.com.fintech.api.sale.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.fintech.api.sale.model.Conta;
import br.com.fintech.api.sale.repository.query.ContaRepositoryQuery;

@Repository
public interface ContaRepository extends JpaRepository<Conta, Long>, ContaRepositoryQuery{

}
