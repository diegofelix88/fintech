package br.com.fintech.api.sale.model.enums;

public enum BandeiraCartaoEnum {

	MASTERCARD,
	VISA
}
