package br.com.fintech.api.sale.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fintech.api.config.errors.AplicationException;
import br.com.fintech.api.generico.service.GenericBaseService;
import br.com.fintech.api.sale.model.Cartao;
import br.com.fintech.api.sale.model.Cliente;
import br.com.fintech.api.sale.model.dto.CartaoDTO;
import br.com.fintech.api.sale.repository.CartaoRepository;

@Service
public class CartaoService extends GenericBaseService<CartaoRepository, Cartao, Long> {
	
	@Autowired
	private ClienteService clienteService;
	
	public Cartao buscarPorNumeroConta(String identificador, String verificador) {
		return repositorio.findBydigitoIdentificadorAndDigitoVerificador(identificador, verificador);
	}
	
	public Cartao saldoCartao(CartaoDTO entity) {
		Cliente cliente = clienteService.saldoCartao(entity.getCpfCliente(), entity.getNumeroIdentificadorCartao(), entity.getNumeroVerificadorCartao());
		
		if(cliente == null) {
	        throw new AplicationException("Cartão não encontrado!");
		}
		Cartao cartao = repositorio.getById(cliente.getConta().getCartao().getId());
		cartao.setLimite(entity.getSaldo());
		
		return repositorio.save(cartao);
	}

}
