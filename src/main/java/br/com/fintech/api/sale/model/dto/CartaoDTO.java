package br.com.fintech.api.sale.model.dto;

import java.math.BigDecimal;

public class CartaoDTO {
	
	private String cpfCliente;
	
	private String numeroIdentificadorCartao;
	
	private String numeroVerificadorCartao;

	private BigDecimal saldo;

	public String getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	public String getNumeroIdentificadorCartao() {
		return numeroIdentificadorCartao;
	}

	public void setNumeroIdentificadorCartao(String numeroIdentificadorCartao) {
		this.numeroIdentificadorCartao = numeroIdentificadorCartao;
	}

	public String getNumeroVerificadorCartao() {
		return numeroVerificadorCartao;
	}

	public void setNumeroVerificadorCartao(String numeroVerificadorCartao) {
		this.numeroVerificadorCartao = numeroVerificadorCartao;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	
}
