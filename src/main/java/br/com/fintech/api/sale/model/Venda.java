package br.com.fintech.api.sale.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.fintech.api.generico.model.GenericBaseModel;

@Entity
@Table(name = "tb_venda", schema = "fintech")
public class Venda extends GenericBaseModel<Long> {

	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "cliente_id", referencedColumnName = "id")
	private Cliente cliente;
	
	@Size(max = 50)
	@Column(name = "servico")
	private String servico;
	
	@Column(name = "valor")
	private BigDecimal valor;

	public Venda() {
		super();
	}

	public Venda(Cliente cliente, @Size(max = 50) String servico, BigDecimal valor) {
		super();
		this.cliente = cliente;
		this.servico = servico;
		this.valor = valor;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getServico() {
		return servico;
	}

	public void setServico(String servico) {
		this.servico = servico;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
}
