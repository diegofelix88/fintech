package br.com.fintech.api.sale.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.fintech.api.sale.model.Cartao;
import br.com.fintech.api.sale.repository.query.CartaoRepositoryQuery;

@Repository
public interface CartaoRepository extends JpaRepository<Cartao, Long>, CartaoRepositoryQuery {

	public Cartao findBydigitoIdentificadorAndDigitoVerificador(String identificador, String verificador);
	
}
